pipeline {
    agent { label 'new-builds' }
    stages {
        stage('build-tag-push') {
            environment {
                AWS_ID = credentials("aws-video-ecr-access-keys")
                AWS_ACCESS_KEY_ID = "${env.AWS_ID_USR}"
                AWS_SECRET_ACCESS_KEY = "${env.AWS_ID_PSW}"
            }
            steps {
                checkout scm
                script {
                    def repoName = env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
                    def imageName = repoName + ":$GIT_BRANCH"
                    sh "docker build . -t $imageName"

                    /*** Push to Local Registry ***/
                    def localRegistry = "docker-local.registry.devinfra.ptec/live"
                    def localImageName = localRegistry + "/" + imageName
                    docker.withRegistry("https://" + localRegistry, "ARTIFACTORY_DEVINFRA_PTEC") {
                        sh """
                            docker tag $imageName $localImageName
                            docker push $localImageName
                        """
                    }

                    /*** Push to ECR ***/
                    sh '''
                        # Disable command echo to hide dynamic docker password
                        set +x
                        \$(docker run --rm \
                            -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
                            -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
                            mesosphere/aws-cli ecr get-login --no-include-email --region eu-west-1)
                        # Turn command echo back on
                        set -x
                    '''
                    def ecrRegistry = "595225974465.dkr.ecr.eu-west-1.amazonaws.com"
                    def ecrImageName = ecrRegistry + "/" + imageName
                    docker.withRegistry("https://" + ecrRegistry) {
                        sh """
                            docker tag $imageName $ecrImageName
                            docker push $ecrImageName
                        """
                    
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                currentBuild.result = currentBuild.result ?: 'SUCCESS'
                notifyBitbucket()
            }
        }
    }
}
