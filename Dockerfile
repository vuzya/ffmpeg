FROM alpine
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
WORKDIR /tmp/ffmpeg
ADD . /tmp/ffmpeg
RUN apk add --no-cache yasm g++ make x264 x264-dev x264-libs fdk-aac fdk-aac-dev lame lame-dev
RUN PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" \
  --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
  --extra-libs="-lpthread -lm" \
  --bindir="$HOME/bin" \
  --enable-gpl \
  --enable-libfdk-aac \
  --enable-libmp3lame \
  --enable-libx264 \
  --enable-nonfree \
  && make

FROM alpine
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
WORKDIR /app/ffmpeg
RUN apk add --no-cache x264 x264 x264-libs fdk-aac fdk-aac lame lame
COPY --from=0 /tmp/ffmpeg/ffmpeg /app/ffmpeg/ffmpeg
ENV PATH="/app/ffmpeg:${PATH}"
